import json
import random
import shutil
import os
from os import listdir
from os.path import isfile

import cv2
import numpy as np
import streamlit as st
from happy_util import grid_detection, find_happywall_panel, four_point_transform, draw_grid

st.sidebar.title('HappyWall v3.0')
st.markdown('## Results')

if os.path.isfile('settings.json'):
    with open('settings.json', 'r') as file:
        print('Loading settings from file...')
        ui_selection = json.load(file)
else:
    print('Loading settings from file...')
    ui_selection = {
        "rotate_option": 2,
        "flip_option": -1,
        "image_crop_x1_size": 100,
        "image_crop_x2_size": 420,
        "image_crop_y1_size": 190,
        "image_crop_y2_size": 100,
        "resize_image_proc": 645,
        "margin_x": 10
    }

st.sidebar.markdown("## Output")
show_advance_output_check = st.sidebar.checkbox("Advance output", value=False)
scale = st.sidebar.number_input("Scale Results", value=ui_selection['results_scale'], min_value=1, max_value=10)

st.sidebar.markdown("## Input")

index_image_check = st.sidebar.checkbox("Index image", value=True)

image_index = st.number_input(
    'Image index',
    value=0,
    min_value=0
)

user_image_file = st.sidebar.text_input('File Name', 'upload_img_20200829-085112.jpg')
button_image_to_showcase = st.sidebar.button("Image to showcase")
showcase_check = st.sidebar.checkbox("Showcase")

st.sidebar.markdown("## Controls")

detection_accuracy = st.sidebar.number_input("Detection Accuracy", value=ui_selection['detection_accuracy'], step=0.01,
                                             min_value=-1.0, max_value=1.0)

rotate_option = st.sidebar.selectbox('How would you like to rotate the image',
                                     (cv2.ROTATE_90_CLOCKWISE, cv2.ROTATE_90_COUNTERCLOCKWISE, cv2.ROTATE_180), 1)
flip_option = st.sidebar.selectbox('How would you like to flip the image', (0, 1, -1), 2)

panel_crop_x1 = st.sidebar.number_input("Crop Left Side", value=ui_selection['image_crop_x1_size'])
panel_crop_x2 = st.sidebar.number_input("Crop Right Side", value=ui_selection['image_crop_x2_size'])
panel_crop_y1 = st.sidebar.number_input("Crop Top Side", value=ui_selection['image_crop_y1_size'])
panel_crop_y2 = st.sidebar.number_input("Crop Bottom Side", value=ui_selection['image_crop_y2_size'])

resize_image_proc = st.sidebar.slider(
    'Resize %',
    100, 1800,
    value=ui_selection['resize_image_proc']
)

# Add a slider to the sidebar:
margin_x = add_slider = st.sidebar.slider(
    'Top Left Margin',
    0, 100,
    value=ui_selection['margin_x']
)

margin_y = add_slider = st.sidebar.slider(
    'Top Upper Margin',
    10, 100,
    value=ui_selection['margin_y']
)

box_width = add_slider = st.sidebar.slider(
    'Box Width',
    0, 100
)

box_height = add_slider = st.sidebar.slider(
    'Box Height',
    0, 100
)

setting_file_name = st.sidebar.text_input('Settings file name', 'settings_3.json')
button_save_settings = st.sidebar.button('Save Settings')


def run_the_app():
    DATA_DIR = 'data'
    print("UI Loop")

    if button_save_settings:
        save_parameters()

    if showcase_check:
        DATA_DIR = 'demo'
        only_files = get_image_files(DATA_DIR)
    else:
        only_files = get_image_files(DATA_DIR)

    image_file = user_image_file
    prev_image_file = user_image_file
    if index_image_check:
        try:
            image_file = only_files[image_index]
            prev_index= image_index + 1
            prev_image_file = only_files[prev_index]
        except:
            image_file = only_files[0]

    print("Image from index: ", image_file)
    st.markdown(body=image_file)
    image_file_full_path = os.path.join(DATA_DIR, image_file)
    prev_image_file_full_path = os.path.join(DATA_DIR, prev_image_file)
    if os.path.lexists(image_file_full_path):
        # Convert to the right colors
        original_image = cv2.cvtColor(cv2.imread(image_file_full_path), cv2.COLOR_BGR2RGB)
        prev_image = cv2.cvtColor(cv2.imread(prev_image_file_full_path), cv2.COLOR_BGR2RGB)
    else:
        print("Can't find the image.")
        return
    
    if button_image_to_showcase:
        shutil.copy(image_file_full_path, 'demo')

    coords, original_rotate_image, crop_image, lines = phase_one_core_transformations(
        original_image,
        rotate_option,
        panel_crop_x1,
        panel_crop_y1,
        panel_crop_x2,
        panel_crop_y2,
        show_advance_output_check)

    black_image2 = 0 * np.ones(shape=crop_image.shape, dtype=np.uint8)
    black_image_result = black_image2

    try:
        if lines.any():
            for line in lines:
                x1, y1, x2, y2 = line[0]
                cv2.rectangle(black_image2, (x1, y1), (x2, y2), (255, 200, 0), 3)
        vector_results, black_image_result, grid_image = grid_detection(img=black_image2,
                                                                        original_image=crop_image.copy(),
                                                                        margin_x=85,
                                                                        margin_y=58,
                                                                        box_x_size=46,
                                                                        box_y_size=23,
                                                                        step_size_x=29,
                                                                        step_size_y=47,
                                                                        results_scale=scale)
    except Exception as e:
        print(e)
        pass

    roi = people_over_panel(original_rotate_image, show_advance_output_check)

    prev_image = rotate_and_flip(prev_image, flip_option, rotate_option)
    o_image = rotate_and_flip(original_image, flip_option, rotate_option)
    # st.image(prev_image, "prev image")

    o_image, _ = crop_with_correct_perspective(o_image,
                                                            panel_crop_x1,
                                                            panel_crop_y1,
                                                            panel_crop_x2,
                                                            panel_crop_y2)

    prev_crop_image, coords = crop_with_correct_perspective(prev_image,
                                             panel_crop_x1,
                                             panel_crop_y1,
                                             panel_crop_x2,
                                             panel_crop_y2)
    if roi:
        for r in roi:
            a = r[0]
            b = r[1]
            st.image(cv2.rectangle(prev_crop_image.copy(), a, b, (0, 255, 0), 4), "ROI without people")
            a_x, a_y = r[0]
            b_x, b_y = r[1]
            o_image[0:b_y, a_x: b_x] = prev_crop_image[0:b_y, a_x: b_x]

        # st.image(prev_crop_image, "prev area to copy/paste")
        st.image(o_image, "Cleanup people from image")

    if show_advance_output_check:
        auto_panel_pt1, auto_panel_pt2 = find_happywall_panel(original_rotate_image, debug=False)
        find_panel_image = original_rotate_image.copy()
        cv2.rectangle(find_panel_image, auto_panel_pt1, auto_panel_pt2, (0, 255, 0), 4)
        st.image(find_panel_image, "Find HappyWall")

        panel_x_pt1, panel_y_pt1 = auto_panel_pt1
        panel_x_pt2, panel_y_pt2 = auto_panel_pt2

        auto_coords = np.array([(panel_x_pt1, panel_y_pt1),
                           (panel_x_pt2, panel_y_pt1),
                           (panel_x_pt1, panel_y_pt2),
                           (panel_x_pt2, panel_y_pt2)])
        print("Auto Panel Coords:", auto_coords)

        try:
            wrap_image = four_point_transform(original_rotate_image, coords)
            st.image(wrap_image, "Transform Perspective (wrap)")
        except:
            print("Cannot transform perspective")

        st.image(black_image_result, caption='Black Background with results')

    vector_results_backround = 40 * np.ones(shape=crop_image.shape, dtype=np.uint8)
    img_height, img_width, _ = vector_results_backround.shape

    vector_results_backround = cv2.resize(vector_results_backround,
                                          (int(img_width / scale), int(img_height / scale) + 20))

    # try:
    exception = False
    for i in vector_results:
        try:
            cv2.rectangle(img=vector_results_backround, pt1=tuple(i[0]), pt2=tuple(i[1]), color=tuple(i[2]),
                          thickness=-1)
        except:
            exception = True

    if exception:
        st.markdown("# ¯\\\_(ツ)_/¯ \n I don't know.")

    print("Shape:", vector_results_backround.shape)

    thickness = 20
    # cv2.rectangle(vector_results_backround,
    #               pt1=(thickness, thickness),
    #               pt2=(img_width-thickness, img_height-thickness),
    #               color=(255, 255, 153),
    #               thickness=thickness)

    st.image(original_rotate_image, caption='Original with flip & rotate')
    st.image(vector_results_backround, caption='Final Vector Results Scaled')
    # except:

    if showcase_check:
        city_img = cities('cities/IASI_03.jpeg', vector_results, xmargin=900, ymargin=620, scale=scale)
        st.image(city_img)
        city_img = cities('cities/TM_03.png', vector_results, xmargin=200, ymargin=250, scale=scale)
        st.image(city_img)
        pass


def rotate_and_flip(original_image, flip_option, rotate_option):
    clip_image = cv2.flip(original_image, flip_option)
    return cv2.rotate(clip_image, rotate_option)


def crop_with_correct_perspective(image, panel_crop_x1, panel_crop_y1, panel_crop_x2, panel_crop_y2):
    image = image.copy()
    if panel_crop_y1 and panel_crop_y2 and panel_crop_x1 and panel_crop_x2:
        coords = np.array([(panel_crop_x1, panel_crop_y1),
                           (panel_crop_x2 + panel_crop_x1, panel_crop_y2 + panel_crop_y1),
                           (panel_crop_x2 + panel_crop_x1, panel_crop_y1 + 2),
                           (panel_crop_x1, panel_crop_y2 + panel_crop_y1)])
        crop_img = four_point_transform(image, coords)
    else:
        crop_img = image.copy()
    return crop_img, coords


def phase_one_core_transformations(original_image, rotate_option, panel_crop_x1, panel_crop_y1, panel_crop_x2, panel_crop_y2, debug=True):

    if rotate_option:
        clip_image = cv2.flip(original_image, flip_option)
        original_rotate_image = cv2.rotate(clip_image, rotate_option)

    crop_img, coords = crop_with_correct_perspective(original_rotate_image, panel_crop_x1, panel_crop_y1, panel_crop_x2, panel_crop_y2)

    width = int(crop_img.shape[1] * resize_image_proc / 100)
    height = int(crop_img.shape[0] * resize_image_proc / 100)
    dim = (width, height)
    # resize image
    crop_img = cv2.resize(crop_img, dim, interpolation=cv2.INTER_AREA)
    gray = cv2.cvtColor(crop_img.copy(), cv2.COLOR_BGR2GRAY)
    kernel = np.ones((4, 4), np.uint8)
    erosion = cv2.erode(gray, kernel, iterations=8)
    edges = cv2.Canny(erosion, 50, 150)
    morph_kernel = np.ones((1, 1), np.uint8)
    morphology = cv2.morphologyEx(edges, cv2.MORPH_CROSS, morph_kernel)
    lines = cv2.HoughLinesP(image=edges, rho=10, theta=np.pi / 180, threshold=10, minLineLength=2, maxLineGap=25)

    if debug:
        st.image(original_rotate_image, caption='Original Image with rotation & flip')
        st.image(draw_grid(crop_img), "Grid")
        st.image(gray, caption='Gray Image')
        st.image(erosion, caption="Erosion")
        st.image(edges, caption='Canny')
        st.image(morphology, caption='Morphology Transformation')

    return coords, original_rotate_image, crop_img, lines


def get_image_files(DATA_DIR):
    only_files = [f for f in listdir(DATA_DIR) if isfile(os.path.join(DATA_DIR, f))]
    only_files.sort(key=lambda x: os.path.getmtime(os.path.join(DATA_DIR, x)))
    print(only_files)
    return only_files


def save_parameters():
    print('Saving user selection.')
    user_ui_selection = {
        'rotate_option': rotate_option,
        'flip_option': flip_option,
        'image_crop_x1_size': panel_crop_x1,
        'image_crop_x2_size': panel_crop_x2,
        'image_crop_y1_size': panel_crop_y1,
        'image_crop_y2_size': panel_crop_y2,
        'resize_image_proc': resize_image_proc,
        'margin_x': margin_x,
        'margin_y': margin_y,
        'results_scale': scale,
        'detection_accuracy': detection_accuracy
    }
    with open(setting_file_name, 'w') as file:
        json.dump(user_ui_selection, file)


def people_over_panel(original_image, debug=False):

    hog = cv2.HOGDescriptor()
    hog.setSVMDetector(cv2.HOGDescriptor_getDefaultPeopleDetector())
    regions, prec = hog.detectMultiScale(original_image,
                                         winStride=(2, 2),
                                         padding=(1, 1),
                                         scale=1.09)
    # Drawing the regions in the Image
    print("regions with people:", regions)
    if debug:
        debug_img = original_image.copy()
        for (x, y, w, h) in regions:
            cv2.rectangle(debug_img, (x, y), (x + w, y + h), (255, 255, 0), 2)
        st.image(debug_img, "People Detector")

    people_area = []
    for (x, y, w, h) in regions:
        if x > panel_crop_x1:
            x1_p = x - panel_crop_x1
            y1_p = y - panel_crop_y1
            x2_p = x + w - panel_crop_x1
            y2_p = y + h - panel_crop_y1

            if y2_p >= panel_crop_y2:
                y2_p = panel_crop_y2

            if x2_p >= panel_crop_x2:
                x2_p = panel_crop_x2
            print("Person", x2_p, y2_p)
            people_area.append([(x1_p, y1_p), (x2_p, y2_p)])
        return people_area


def cities(img_name, vector_results, xmargin, ymargin, scale):
    city_img = cv2.imread(img_name)
    city_img = cv2.cvtColor(city_img, cv2.COLOR_BGR2RGB)
    for i in vector_results:
        print("city:", i[0], i[1])

        x_margin = xmargin
        y_margin = ymargin

        pt1 = (i[0][0] + x_margin, i[0][1] + y_margin)
        pt2 = (i[0][0] + x_margin + 5, i[0][1] + y_margin + 10)
        # pt2 = (i[1][0] + x_margin, i[1][1] + y_margin)
        rect_color = i[2]

        font = cv2.FONT_HERSHEY_SIMPLEX
        # cv2.putText(original_image, f'*', pt1, font, 0.5, (0, 255, 0), 2, cv2.LINE_AA)

        if scale == 1:
            h = 21
            w = 37
        else:
            h = 21
            w = 37
        try:
            # sub_img = city_img[i[1][1] + y_margin:i[1][1] + y_margin + h, i[0][0] + x_margin:i[0][0] + x_margin + w]
            # white_rect = np.ones(sub_img.shape, dtype=np.uint8) * 255
            #
            # res = cv2.addWeighted(sub_img, 0.5, white_rect, 0.5, 1.0)
            # city_img[i[1][1] + y_margin:i[1][1] + y_margin + h, i[0][0] + x_margin:i[0][0] + x_margin + w] = res
            pass
        except:
            pass
        cv2.rectangle(img=city_img, pt1=pt1, pt2=pt2,
                      color=(0, 255, 0),
                      thickness=-1)

    return city_img


def load_values_into_ui(json_settings):
    pass


def main():
    print("Main...")
    run_the_app()


if __name__ == "__main__":
    main()
