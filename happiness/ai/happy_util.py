import cv2
from pandas import np
import streamlit as st
import numpy as pts


def green(img):
    img = img.copy()
    if img.any():
        grid_RGB = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        grid_HSV = cv2.cvtColor(grid_RGB, cv2.COLOR_RGB2HSV)
        lower_green = np.array([25, 52, 72])
        upper_green = np.array([102, 255, 255])
        mask = cv2.inRange(grid_HSV, lower_green, upper_green)
        green_perc = (np.sum(mask) / np.size(mask)) / 255
        return green_perc
    else:
        return 0


def find_happywall_panel(img, debug=False):
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    blur = cv2.GaussianBlur(gray, (1, 1), sigmaX=10)
    canny = cv2.Canny(blur, 500, 150, apertureSize=3)
    if debug:
        st.image(canny)

    _, thresh = cv2.threshold(canny, 120, 300, 0)
    contours, _ = cv2.findContours(thresh, 1, cv2.CHAIN_APPROX_NONE)
    for cnt in contours:
        rect = cv2.boundingRect(cnt)
        x, y, w, h = rect
        # get the largest object
        if w * h > 20000:
            print("panel w & h: ", w, h)
            return (x, y), (x + w, y + h)
    return (0, 0), (10, 10)


def grid_detection(img, original_image, line_color=(0, 255, 0), margin_x=0, margin_y=0, step_size_x=29, step_size_y=47,
                   box_y_size=46,
                   box_x_size=23, results_scale=1, detection_accuracy=0.06):
    print("detection_accuracy", detection_accuracy)

    vector_results = []
    for x in range(0, img.shape[1] - margin_x, step_size_x):
        x = x + margin_x
        for y in range(0, img.shape[0] - margin_y, step_size_y):
            y = y + margin_y
            if x<img.shape[1] - margin_x and y < img.shape[0] - margin_y:
                a = img[y: y + box_y_size - 1, x: x + box_x_size - 1]
                g = green(a)
                if g > 0.0:
                    try:
                        pixel_color = original_image[y + 15, x + 25]
                        c = (int(pixel_color[0]), int(pixel_color[1]), int(pixel_color[2]))
                        r, g, b = c
                        # if color is less than some dark nuance
                        if r <= 50 and g <= 61 and b <= 77:
                            c = (40, 40, 40)
                        cv2.circle(img, (x+15, y+25), 10, (255, 0, 0), 2)
                    except Exception as e:
                        print("get color exception:", e)
                    # cv2.circle(img, (x, y), 10, (0, 255, 0), 1)
                    cv2.rectangle(img, pt1=(x, y), pt2=(x+23, y+46), color=line_color, thickness=1)
                    # cv2.rectangle(img=original_image, pt1=(x, y), pt2=(x + box_x_size, y+box_y_size),
                    #               color=c, thickness=5)
                    vector_results.append((
                    (int(x/results_scale), int(y/results_scale)),
                    (int((x + 21)/results_scale), int((y + 37)/results_scale)),c))

    return vector_results, img, original_image


def draw_grid(image, line_color=(0, 255, 0), margin_x=85, margin_y=58, step_size_x=29, step_size_y=47,
              box_y_size=46,
              box_x_size=23, results_scale=1, detection_accuracy=0.0006):
    print("detection_accuracy", detection_accuracy)
    img = image.copy()
    c = 1
    for x in range(0, img.shape[1] - margin_x, step_size_x):
        x = x + margin_x
        c = c * -1
        for y in range(0, img.shape[0] - margin_y, step_size_y):
            y = y + margin_y
            if x < img.shape[1] - margin_x and y < img.shape[0] - margin_y:
                if c == 1:
                    line_color = (255, 255, 255)
                else:
                    line_color = (0, 255, 0)

                cv2.rectangle(img, (x, y + box_y_size), (x + box_x_size, y), color=line_color, thickness=1)
    return img


def order_points(pts):
    rect = np.zeros((4, 2), dtype="float32")
    s = pts.sum(axis=1)
    rect[0] = pts[np.argmin(s)]
    rect[2] = pts[np.argmax(s)]
    diff = np.diff(pts, axis=1)
    rect[1] = pts[np.argmin(diff)]
    rect[3] = pts[np.argmax(diff)]
    return rect


def four_point_transform(image, pts):
    rect = order_points(pts)
    (tl, tr, br, bl) = rect
    width_A = np.sqrt(((br[0] - bl[0]) ** 2) + ((br[1] - bl[1]) ** 2))
    width_B = np.sqrt(((tr[0] - tl[0]) ** 2) + ((tr[1] - tl[1]) ** 2))
    max_Width = max(int(width_A), int(width_B))
    height_A = np.sqrt(((tr[0] - br[0]) ** 2) + ((tr[1] - br[1]) ** 2))
    height_B = np.sqrt(((tl[0] - bl[0]) ** 2) + ((tl[1] - bl[1]) ** 2))
    max_Height = max(int(height_A), int(height_B))
    dst = np.array([
        [0, 0],
        [max_Width - 1, 0],
        [max_Width - 1, max_Height - 1],
        [0, max_Height - 1]], dtype="float32")
    M = cv2.getPerspectiveTransform(rect, dst)
    warped = cv2.warpPerspective(image, M, (max_Width, max_Height))
    return warped
